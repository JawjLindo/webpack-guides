import {cube} from './math';

if (process.env.NODE_ENV !== 'production') {
  console.log('Looks like we are in development mode');
}

function component() {
  let element = document.createElement('pre');

  // Lodash, currently included via a script, is required for this line to work
  element.innerHTML = [
    'Hello webpack!',
    '5 cube is equal to ' + cube(5)
  ].join('\n\n');

  return element;
}

document.body.appendChild(component());
